# -*- coding: utf-8 -*-

from django.views.generic.edit import FormView
from revfront.revusers.forms import ParticipantForm, ParticipantLoginForm
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


class RegistrationView(FormView):
    """Participant registration view."""

    template_name = "revusers/registration.html"
    form_class = ParticipantForm
    success_url = reverse_lazy("revfront:revusers:registration")

    def get_context_data(self, **kwargs):
        """Overrides context to add menu."""

        context = super(RegistrationView, self).get_context_data(**kwargs)
        context['menu'] = 'payment'

        return context

    def form_valid(self, form):
        """Save User and Participant objects."""

        form.save()
        messages.success(self.request, _(u"¡Te has registrado correctamente!"))

        return super(RegistrationView, self).form_valid(form)


class LoginView(FormView):
    """Participant login view."""

    template_name = "revusers/login.html"
    form_class = ParticipantLoginForm
    success_url = reverse_lazy("revfront:index")

    def get_context_data(self, **kwargs):
        """Overrides context to add menu."""

        context = super(LoginView, self).get_context_data(**kwargs)
        context['menu'] = 'revusers:login'

        return context

    def form_valid(self, form):
        """Save User and Participant objects."""

        logout(self.request)
        login_ok = form.login(self.request)

        if not login_ok:
            messages.warning(self.request, _(u"El usuario o el password son inválidos"))
            return redirect("revfront:revusers:login")

        return super(LoginView, self).form_valid(form)


@login_required
def logout_view(request):
    """Logout view."""

    logout(request)
    return redirect("revfront:revusers:login")