# -*- coding: utf-8 -*-

import revfront.revusers.views

from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^registration/$', revfront.revusers.views.RegistrationView.as_view(), name='registration'),
    url(r'^login/$', revfront.revusers.views.LoginView.as_view(), name='login'),
    url(r'^logout/$', revfront.revusers.views.logout_view, name='logout')
)