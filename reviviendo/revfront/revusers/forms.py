# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from revfront.revusers.models import Participant
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


class ParticipantLoginForm(forms.Form):
    """Form to handle participant login."""

    username = forms.CharField(max_length=128, required=True, label=_("Nombre de usuario"))
    password = forms.CharField(max_length=128, required=True, label=_("Password"), widget=forms.PasswordInput)

    def login(self, request):
        """Attempts to login the user."""

        user = authenticate(username=self.cleaned_data["username"], password=self.cleaned_data["password"])

        if user is not None:
            if user.is_active:
                try:
                    Participant.objects.get(user=user)
                    login(request, user)
                    return True
                except Participant.DoesNotExist:
                    pass

        return False


class ParticipantForm(UserCreationForm):
    """Custom UserCreationForm that creates an associated Participant."""

    first_name = forms.CharField(max_length=30, required=True, label=_("Nombre"))
    last_name = forms.CharField(max_length=30, required=True, label=_("Apellidos"))
    email = forms.EmailField(required=True, label=_("Email"))
    dni = forms.CharField(max_length=16, required=True, label=_("DNI"))
    city = forms.CharField(max_length=64, required=True, label=_("Ciudad"))
    phone = forms.CharField(max_length=16, required=True, label=_(u"Teléfono"))
    birthdate = forms.DateField(
        required=True, label=_("Fecha de nacimiento"),
        widget=forms.DateInput(attrs={"placeholder": _("dd/mm/aaaa")}))
    comments = forms.CharField(required=False, widget=forms.Textarea(attrs={
        "placeholder": _("Alergias, restricciones alimentarias u otros comentarios que consideres oportunos.")
    }), label=_("Comentarios"))
    organizer = forms.BooleanField(required=False, label=_(u"¿Quieres organizar un ReV?"))

    class Meta:
        """Meta attributes."""

        model = User
        fields = (
            "username", "first_name", "last_name", "email", "password1",
            "password2", "dni", "city", "phone", "birthdate", "comments", "organizer")

    def clean_dni(self):
        """Check that this DNI is unique."""

        try:
            Participant.objects.get(dni=self.cleaned_data["dni"])
            raise ValidationError(
                _('Ya existe un participante con DNI: %(dni)s'),
                code='uniquedni',
                params={'dni': self.cleaned_data["dni"]})
        except Participant.DoesNotExist:
            pass

        return self.cleaned_data["dni"]

    def save(self, commit=True):
        """Override parent save() method to add Participant relationship."""

        user = super(ParticipantForm, self).save(commit=False)
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.email = self.cleaned_data["email"]

        if commit:
            user.save()

        participant = Participant(
            dni=self.cleaned_data["dni"], city=self.cleaned_data["city"], phone=self.cleaned_data["phone"],
            birthdate=self.cleaned_data["birthdate"], comments=self.cleaned_data["comments"], user=user,
            organizer=self.cleaned_data["organizer"])

        if commit:
            participant.save()

        return user