# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Participant(models.Model):
    """Person that attends the event."""

    class Meta:
        """Meta attributes."""

        verbose_name = _("Participante".decode("utf8"))
        verbose_name_plural = _("Participantes".decode("utf8"))

    user = models.OneToOneField(User, verbose_name=_("Usuario asociado"))
    dni = models.CharField(max_length=16, unique=True, verbose_name=_("DNI"))
    city = models.CharField(max_length=64, verbose_name=_("Ciudad"))
    phone = models.CharField(max_length=16, verbose_name=_(u"Teléfono"))
    birthdate = models.DateField(verbose_name=_("Fecha de nacimiento"))
    comments = models.TextField(blank=True, null=True, verbose_name=_("Comentarios"))
    payment = models.BooleanField(default=False, verbose_name=_("Pago"))
    organizer = models.BooleanField(default=False, verbose_name=_("Desea organizar"))

    def __unicode__(self):
        """Object's unicode representation."""

        return u'{0}, {1} ({2})'.format(self.user.last_name, self.user.first_name, self.user.email)