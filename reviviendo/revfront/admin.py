# -*- coding: utf-8 -*-

from django.contrib import admin
from revfront.revevents.models import *
from revfront.revusers.models import *
from django.utils.translation import ugettext_lazy as _


def participant_user_full_name(participant):
    """Returns full name of User associated with Participant."""

    return u"{0} {1}".format(participant.user.first_name, participant.user.last_name)


participant_user_full_name.short_description = _("Nombre y Apellidos")


def participant_user_email(participant):
    """Returns email of User associated with Participant."""

    return participant.user.email


participant_user_email.short_description = _("Email")


class ParticipantAdmin(admin.ModelAdmin):
    """ModelAdmin of Participant."""

    list_display = (
        participant_user_full_name, participant_user_email,
        "dni", "city", "phone", "birthdate", "comments", "payment", "organizer")


def activity_free_places(activity):
    """Returns Activity free places."""

    return activity.free_places()


activity_free_places.short_description = _("Plazas libres")


def activity_participants(activity):
    """Returns formatted list of Activity Participants."""

    participants = activity.participants.all().order_by("user__last_name")

    if not len(participants):
        return _(u"Ningún participante")

    return "<ul>{0}</ul>".format("".join(["<li>{0}</li>".format(p) for p in participants]))


activity_participants.short_description = _("Lista de jugadores")
activity_participants.allow_tags = True


class ActivityAdmin(admin.ModelAdmin):
    """ModelAdmin of ActivityAdmin."""

    list_display = ("name", "dtime", "max_participants", activity_free_places, activity_participants)


admin.site.register(Participant, ParticipantAdmin)
admin.site.register(Activity, ActivityAdmin)
