# -*- coding: utf-8 -*-

from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    """Homepage index view."""

    template_name = "revfront/index.html"

    def get_context_data(self, **kwargs):
        """Overrides context to add menu."""

        context = super(IndexView, self).get_context_data(**kwargs)
        context['menu'] = 'index'

        return context


class PaymentView(TemplateView):
    """Payment info view."""

    template_name = "revfront/payment.html"

    def get_context_data(self, **kwargs):
        """Overrides context to add menu."""

        context = super(PaymentView, self).get_context_data(**kwargs)
        context['menu'] = 'payment'

        return context


class FaqsView(TemplateView):
    """Frequently asked questions view."""

    template_name = "revfront/faqs.html"

    def get_context_data(self, **kwargs):
        """Overrides context to add menu."""

        context = super(FaqsView, self).get_context_data(**kwargs)
        context['menu'] = 'faqs'

        return context