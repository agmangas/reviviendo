# -*- coding: utf-8 -*-

from django.views.generic.list import ListView
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from revfront.revevents.models import Activity
from django.contrib.auth.decorators import login_required
from revfront.revusers.models import Participant


class ActivityListView(ListView):
    """List view for the Activity model."""

    model = Activity

    def dispatch(self, request, *args, **kwargs):
        """Override dispatch to logout admins that enter the activity page."""

        if self.request.user.is_authenticated():
            try:
                self.request.user.participant
            except Participant.DoesNotExist:
                return redirect("revfront:revusers:logout")

        return super(ActivityListView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Overrides context to add menu and participant info."""

        activities = self.get_queryset()

        context = super(ActivityListView, self).get_context_data(**kwargs)
        context['menu'] = 'revevents:list'
        context['subscriptions'] = dict()

        if self.request.user.is_authenticated():
            participant = self.request.user.participant
            context['subscriptions'] = dict((act.pk, act.has_participant(participant)) for act in activities)

        return context

    def get_queryset(self):
        """Override queryset to order by datetime."""

        return Activity.objects.all().order_by("dtime")


@login_required
def sign_up_view(request, activity_pk):
    """View to handle Participants signing up for Activities."""

    activity = get_object_or_404(Activity, pk=activity_pk)

    if not request.user.participant.payment:
        raise Http404("Cannot sign up without accepted payment")

    if activity.has_participant(request.user.participant):
        raise Http404("Participant is already in the Activity")

    if not activity.free_places():
        raise Http404("Attempt to sign up for full Activity")

    activity.participants.add(request.user.participant)
    activity.save()

    return redirect("revfront:revevents:list")