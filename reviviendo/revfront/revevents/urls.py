# -*- coding: utf-8 -*-

import revfront.revevents.views

from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^list/$', revfront.revevents.views.ActivityListView.as_view(), name='list'),
    url(r'^signup/(?P<activity_pk>[0-9]+)/$', revfront.revevents.views.sign_up_view, name='signup')
)