# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from revfront.revusers.models import Participant


class Activity(models.Model):
    """Activities that are organized in the event and may have associated Participants."""

    class Meta:
        """Meta attributes."""

        verbose_name = _(u"Actividad")
        verbose_name_plural = _(u"Actividades")

    name = models.CharField(max_length=64, unique=True, verbose_name=_(u"Nombre"))
    description = models.TextField(verbose_name=_(u"Descripción"))
    dtime = models.DateTimeField(verbose_name=_(u"Fecha y hora"))
    max_participants = models.IntegerField(verbose_name=_(u"Número de jugadores"))
    comments = models.TextField(null=True, blank=True, verbose_name=_(u"Otros comentarios"))
    participants = models.ManyToManyField(Participant, blank=True)
    image_url = models.CharField(max_length=256, verbose_name=_(u"Imagen (URL)"))

    def __unicode__(self):
        """Object's unicode representation."""

        return u'{0}'.format(self.name)

    def free_places(self):
        """Returns the number of free places for this activity."""

        return self.max_participants - len(self.participants.all())

    def has_participant(self, participant):
        """Returns true if the Participant is already in the Activity."""

        return True if participant in self.participants.all() else False