# -*- coding: utf-8 -*-

import revfront.views

from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    url(r'^$', revfront.views.IndexView.as_view(), name='index'),
    url(r'^registration/$', revfront.views.PaymentView.as_view(), name='payment'),
    url(r'^faqs/$', revfront.views.FaqsView.as_view(), name='faqs'),
    url(r'^events/', include('revfront.revevents.urls', namespace="revevents")),
    url(r'^users/', include('revfront.revusers.urls', namespace="revusers"))
)