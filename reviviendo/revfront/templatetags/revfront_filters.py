# -*- coding: utf-8 -*-

from django import template

register = template.Library()


def access(value, arg):
    """Access dict by variable key"""

    try:
        return value[arg]
    except KeyError:
        return None


register.filter('access', access)