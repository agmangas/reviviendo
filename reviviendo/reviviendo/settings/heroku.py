"""
Heroku production app settings
"""

import dj_database_url

# Parse database configuration from $DATABASE_URL
# Create environment variable on dev machine:
# export DATABASE_URL="postgres://user:passwd@localhost:5432/dbname"

DATABASES = {
    'default': dj_database_url.config()
}

# Honor the 'X-Forwarded-Proto' header for request.is_secure()

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Turn off DEBUG mode

DEBUG = False

TEMPLATE_DEBUG = DEBUG

# Allow all host headers

ALLOWED_HOSTS = ['*']