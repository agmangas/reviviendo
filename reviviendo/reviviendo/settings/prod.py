"""
Production app settings
"""

import os

# Turn off DEBUG mode

DEBUG = False

# Secret key

SECRET_KEY = os.environ['REV_SECRET_KEY']

# Configure prod database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ['REV_DB_NAME'],
        'USER': os.environ['REV_DB_USER'],
        'PASSWORD': os.environ['REV_DB_PASS'],
        'HOST': os.environ['REV_DB_HOST'],
        'PORT': os.environ['REV_DB_PORT']
    }
}