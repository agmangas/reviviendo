"""
Development app settings
"""

# Turn on DEBUG mode

DEBUG = True

TEMPLATE_DEBUG = DEBUG

# Configure dev database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'reviviendo',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}