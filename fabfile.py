#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reviviendo.settings")

from fabric.api import *
from django.conf import settings as conf


def reset_mysql_db():
    """Reset MySQL database."""

    with settings(warn_only=True):
        local("mysql --user={user} --password={pasw} --execute='DROP DATABASE {name}';".format(
            user=conf.DATABASES["default"]["USER"],
            pasw=conf.DATABASES["default"]["PASSWORD"],
            name=conf.DATABASES["default"]["NAME"]))

    local("mysql --user={user} --password={pasw} --execute='CREATE DATABASE {name}';".format(
        user=conf.DATABASES["default"]["USER"],
        pasw=conf.DATABASES["default"]["PASSWORD"],
        name=conf.DATABASES["default"]["NAME"]))


def syncdb():
    """Execute Django syncdb."""

    local("./reviviendo/manage.py syncdb")


def load_fixtures():
    """Load data fixtures."""

    # local("./cellwatcher/manage.py loaddata fixture.json")

    pass


def dev_deployment():
    """Deploy a development instance."""

    reset_mysql_db()
    syncdb()
    load_fixtures()